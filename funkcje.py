import random
from config import *

def bubble_sort(listenka):
    for i in range(len(listenka) - 1):
        for j in range(len(listenka) - 1 - i):
            if listenka[j] > listenka[j + 1]:
                listenka[j],listenka[j + 1] = listenka[j + 1], listenka[j]
    return listenka

def binary_search(x, listewka):
    listewka = bubble_sort(listewka)
    a = 0
    b = len(listewka) - 1
    while a <= b :
        c = ( a + b ) // 2
        if x == listewka[c]:
            return True
        elif x > listewka[c]:
            a = c + 1
        elif x < listewka[c]:
            b = c - 1
    return False

def dodaj_do_slownika(slownik,klucz,wartosc):
    try:
        assert isinstance(slownik,dict)
    except:
        return None
    slownik[klucz]=wartosc

def losuj_pozycje_ciezaru():
    czy_sukces = False
    while(czy_sukces == False):
        losX = random.randint(0,SCREEN_WIDTH-100)
        losY = random.randint(0,SCREEN_HEIGHT-100)
        if losY not in ((SCREEN_HEIGHT//2)-150,(SCREEN_HEIGHT//2)+50) and losX not in ((SCREEN_WIDTH//2)-150,(SCREEN_WIDTH//2)+50):
            czy_sukces = True
    zwracana = [losX,losY]
    return zwracana

def plik_tabelka(nazwapliku):
    zwracana_tabelka = []
    pierwsza_linijka_wczytana = False
    with open(nazwapliku, "r") as otworzony_plik:
        otworzony_plik.seek(0)
        for line in otworzony_plik:
            # pierwsza linijka to naglowki
            if pierwsza_linijka_wczytana == False:
                linijka_naglowkow = line
                naglowki = linijka_naglowkow.split()
                pierwsza_linijka_wczytana = True
            else:
                linijka_tekstu = line
                komorki = linijka_tekstu.split()
                #stworzenie slownika pierwsza_komorka{}
                nazwa_slownik = komorki[0]
                nazwa_slownik = {}
                for i in range(1,len(naglowki),1):
                    nazwa_slownik[naglowki[i]] = komorki[i]
                zwracana_tabelka.append({komorki[0]:nazwa_slownik})
        return zwracana_tabelka

def ustal_moc_rzutu(wspolrzedna_x):
     x = ((wspolrzedna_x-450)/100)
     y = -(abs(8*x)) + 20
     return y

def dodaj_malejacoB(lista,slownik_do_dodania,sortujaca):
    for kluczyk in slownik_do_dodania:
        klucz_slownika_do_dodania = kluczyk
    print(kluczyk)
    zamieniono_elementy_listy = False
    for slowniki in lista:
        if zamieniono_elementy_listy == True:
            break
        for nazwa in slowniki:
            if zamieniono_elementy_listy == True:
                break
            for naglowek in slowniki[nazwa]:
                if zamieniono_elementy_listy == True:
                    break
                if naglowek == sortujaca:
                    indeks_listy = lista.index(slowniki)
                    if int(slowniki[nazwa][naglowek]) <= int(slownik_do_dodania[kluczyk][sortujaca]):
                        lista.insert(indeks_listy,slownik_do_dodania)
                        zamieniono_elementy_listy = True
    if zamieniono_elementy_listy == False:
        lista.append(slownik_do_dodania)
    return lista
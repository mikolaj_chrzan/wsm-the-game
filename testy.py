import pytest
from funkcje import *
import random
import math
from config import *

losowa_lista_do_testow = []
for i in range(10):
    losowa_lista_do_testow.append(random.randint(-100, 100))
print(losowa_lista_do_testow)


def test_bubble():
    assert bubble_sort(losowa_lista_do_testow) == sorted(losowa_lista_do_testow)


def test_binary_search():
    for elem in losowa_lista_do_testow:
        assert binary_search(elem,losowa_lista_do_testow) == True
        print(elem, " jest w tablicy")


def test_ustal_moc_rzutu():
    #sprawdzamy przedzial
    assert ustal_moc_rzutu(450) == 20
    assert ustal_moc_rzutu(200) == 0 and ustal_moc_rzutu(700) == 0
    for argument in range(350,701,1):
        print("test funkcji dla argumentu", argument)
        assert ustal_moc_rzutu(argument) == -(abs( 8*((argument-450)/100) )) + 20


def test_plik_tabelka():
    odczyt = plik_tabelka("przyklad.txt")
    #assert odczyt[0] == "przyklad
    assert odczyt[0]["przyklad"]["suma"] == "100"


def test_losuj_pozycje():
    assert losuj_pozycje_ciezaru()[0] is not (400,500)
    assert losuj_pozycje_ciezaru()[1] is not (300,400)

def test_dodaj_malejacaB():
    lista = plik_tabelka("przyklad.txt")
    arg_poczatek = {"dobry_gracz" : {"konkurencja1" : "320","konkurencja2" : "100","suma" : "420"}}
    arg_srodek = {"sredni_gracz" : {"konkurencja1" : "30","konkurencja2" : "40","suma" : "70"}}
    arg_koniec = {"slaby_gracz" : {"konkurencja1" : "0","konkurencja2" : "0","suma" : "0"}}
    sortujacy_argument = "suma"

    nowa_lista = dodaj_malejacoB(lista, arg_poczatek, sortujacy_argument)
    assert nowa_lista == [{'dobry_gracz': {'konkurencja1': '320', 'konkurencja2': '100', 'suma': '420'}}, {'przyklad': {'kon1': '13', 'kon2': '87', 'suma': '100'}}, {'cobra': {'kon1': '12', 'kon2': '77', 'suma': '89'}}, {'bonzo': {'kon1': '30', 'kon2': '20', 'suma': '50'}}]
    nowa_lista = dodaj_malejacoB(lista, arg_srodek, sortujacy_argument)
    assert nowa_lista == [{'dobry_gracz': {'konkurencja1': '320', 'konkurencja2': '100', 'suma': '420'}}, {'przyklad': {'kon1': '13', 'kon2': '87', 'suma': '100'}}, {'cobra': {'kon1': '12', 'kon2': '77', 'suma': '89'}},{"sredni_gracz" : {"konkurencja1" : "30","konkurencja2" : "40","suma" : "70"}}, {'bonzo': {'kon1': '30', 'kon2': '20', 'suma': '50'}}]
    nowa_lista = dodaj_malejacoB(lista, arg_koniec, sortujacy_argument)
    assert nowa_lista == [{'dobry_gracz': {'konkurencja1': '320', 'konkurencja2': '100', 'suma': '420'}}, {'przyklad': {'kon1': '13', 'kon2': '87', 'suma': '100'}}, {'cobra': {'kon1': '12', 'kon2': '77', 'suma': '89'}},{"sredni_gracz" : {"konkurencja1" : "30","konkurencja2" : "40","suma" : "70"}}, {'bonzo': {'kon1': '30', 'kon2': '20', 'suma': '50'}},{"slaby_gracz" : {"konkurencja1" : "0","konkurencja2" : "0","suma" : "0"}}]

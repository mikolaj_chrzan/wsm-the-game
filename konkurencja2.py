import pygame
import sys
import os
from funkcje import *
def konkurencja2():
    wynik = 0
    screen = pygame.display.set_mode((SCREEN_WIDTH, SCREEN_HEIGHT))
    pygame.display.set_caption("RZUT KULA")
    clock = pygame.time.Clock()
    spowolnienie_czasu = 1
    resource_folder = os.path.join(os.path.dirname(__file__), "zasoby")
    rzucajacy = pygame.image.load(os.path.join(resource_folder, "rzucajacy2.png"))
    rzucajacy.set_colorkey(WHITE)
    court_rect = pygame.Rect(0,400,SCREEN_WIDTH,250)
    grass_rect = pygame.Rect(0,370,SCREEN_WIDTH,400)
    white_line_rect = pygame.Rect(0,500,SCREEN_WIDTH,5)
    x_pasek = SCREEN_WIDTH // 2 - 5
    # lot kuli konfiguracja poczatkowa
    kula = {"x" : 30,"promien" : 30}
    czy_rzucono = False
    a = 1
    b = 300
    kula["y"] = (a * kula["x"]) + b
    speed = 0
    opor = 0.08
    przysp_graw = 1.2
    dol = 0
    tarcie = 1
    czy_w_powietrzu = True
    uderzenie = False
    #==============================================petla
    prawo = True
    RUN = True
    while RUN:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                sys.exit()
    ############################# warunki
        keys = pygame.key.get_pressed()
        if keys[pygame.K_SPACE]:
            speed = ustal_moc_rzutu(x_pasek+5)
            czy_rzucono = True

        if czy_rzucono == True:
            if kula["y"] < SCREEN_HEIGHT // 2 + 100:
                czy_w_powietrzu = True
                uderzenie = False
            elif kula["y"] > SCREEN_HEIGHT // 2 + 100:
                czy_w_powietrzu = False
                uderzenie = True

            if (czy_w_powietrzu == True):
                kula["x"] += speed
                kula["y"] -= speed
                if speed > 0:
                    speed -= opor
                if True:
                    kula["y"] += dol
                    dol += przysp_graw

            if uderzenie == True:
                kula["x"] += speed
                kula["y"] -= speed
                if speed > 0:
                    speed -= opor
                if speed > 1:
                    speed -= tarcie
                if speed < 0.5:
                    speed = 0
    ############################# wyswietlanie
        screen.fill(SKY_BLUE)
        pygame.draw.rect(screen,GRASS_GREEN,grass_rect)
        pygame.draw.rect(screen,LIGHT_RED,court_rect)
        pygame.draw.rect(screen,WHITE,white_line_rect)
        screen.blit(rzucajacy, (0, SCREEN_HEIGHT//2-35))

        pygame.draw.circle(screen,GRAY,(kula["x"],kula["y"]),kula["promien"])
        if czy_rzucono == False:
            pygame.draw.rect(screen, BLACK,(SCREEN_WIDTH//2 -255 , 98.5, 510,55))
            pygame.draw.rect(screen, RED, (SCREEN_WIDTH //2-250, 100, 500, 50))
            pygame.draw.rect(screen,ORANGE,(SCREEN_WIDTH//2-125 ,100,250,50))
            pygame.draw.rect(screen,YELLOW,(SCREEN_WIDTH//2-62.5,100,125,50))
            pygame.draw.rect(screen, GREEN, (SCREEN_WIDTH //2-31.25, 100, 62.5, 50))
            pygame.draw.rect(screen,BLACK,(x_pasek,100,10,50))
            if x_pasek < 200 and prawo == False:
                prawo = True
            elif x_pasek > 700 and prawo == True:
                prawo = False
            if prawo == True:
                x_pasek += SZYBKOSC_PASKA
            elif prawo == False:
                x_pasek -= SZYBKOSC_PASKA
        wynik = (kula["x"] // 10) - 3
        if czy_rzucono == True:
            if(spowolnienie_czasu<45):
                spowolnienie_czasu+=1
            if speed <= 0:
                return wynik
        pygame.display.update()
        clock.tick(FPS - spowolnienie_czasu)

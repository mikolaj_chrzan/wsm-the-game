import pygame
import sys
import os
from funkcje import *
def konkurencja1():
    wynik = 0
    czas = SCREEN_WIDTH
    uplyw_czasu = 1
    screen = pygame.display.set_mode((SCREEN_WIDTH, SCREEN_HEIGHT))
    pygame.display.set_caption("PODNOSZENIE CIEZAROW")
    clock = pygame.time.Clock()
    wynik_font = pygame.font.Font(None, 50)
    resource_folder = os.path.join(os.path.dirname(__file__), "zasoby")
    ########## konfiguracja poczatkowa
    player = {
    "picture" : pygame.image.load(os.path.join(resource_folder, "gracz1.png")) ,
    "x" : 390,
    "y" : 250,
    "speed" : 7 ,
    "czy_podniesiono" : False,
    "bonus_speed" : 0}
    dodaj_do_slownika(player,"rect",player["picture"].get_rect())
    player["picture"].set_colorkey(WHITE)

    ciezar = {"picture" : pygame.image.load(os.path.join(resource_folder, "ciezar.png")),
    "x": losuj_pozycje_ciezaru()[0],
    "y": losuj_pozycje_ciezaru()[1],}
    dodaj_do_slownika(ciezar,"rect",ciezar["picture"].get_rect())
    ciezar["picture"].set_colorkey(WHITE)

    pole_doniesienia_rect = pygame.Rect(SCREEN_WIDTH/2-50,SCREEN_HEIGHT/2-50,100,100)
    piach_rect = pygame.Rect(0, 50, SCREEN_WIDTH, SCREEN_HEIGHT * 0.95)
    #============================================================================================ PETLA
    RUN = True
    while RUN:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                sys.exit()
        ######################## ruch klawiszowy
        keys = pygame.key.get_pressed()
        if keys[pygame.K_UP]:
            player["y"] -= (player["speed"]+player["bonus_speed"])
        if keys[pygame.K_DOWN]:
            player["y"] += (player["speed"]+player["bonus_speed"])
        if keys[pygame.K_RIGHT]:
            player["x"] += (player["speed"]+player["bonus_speed"])
        if keys[pygame.K_LEFT]:
            player["x"] -= (player["speed"]+player["bonus_speed"])
        ####################### warunki
        #kolizja player-ciezar
        if player["rect"].colliderect(ciezar["rect"]):
            kolizja_player_ciezar = True
            #print("kolizja player-ciezar")
        else:
            kolizja_player_ciezar = False
        #podnoszenie
        if kolizja_player_ciezar == True:
            if keys[pygame.K_SPACE]:
                player["czy_podniesiono"] = True
        if player["czy_podniesiono"] == True:
            ciezar["x"], ciezar["y"] = player["x"]+20, player["y"]+60
            player["speed"] = 4
        else:
            player["speed"] = 10
        #kolizja ciezar-obszar
        if ciezar["rect"].colliderect(pole_doniesienia_rect):
            kolizja_ciezar_pole = True
            #print("kolizja ciezar-pole")
        else:
            kolizja_ciezar_pole = False
        if kolizja_ciezar_pole == True:
            wynik += 1
            player["bonus_speed"] += 1
            czas += 50+uplyw_czasu
            ciezar["x"] = losuj_pozycje_ciezaru()[0]
            ciezar["y"] = losuj_pozycje_ciezaru()[1]
            player["czy_podniesiono"] = False
            kolizja_ciezar_pole = False
        ####################### wyswietlanie
        screen.fill(WATER_BLUE)
        pygame.draw.rect(screen, SAND_YELLOW, piach_rect)
        pygame.draw.rect(screen, GREEN, pole_doniesienia_rect)
        screen.blit(player["picture"],(player["x"],player["y"]))
        screen.blit(ciezar["picture"], (ciezar["x"],ciezar["y"]))
        pygame.draw.rect(screen, RED, (0, 0, czas, 50))
        text_surface = wynik_font.render("zdobyte punkty: "+str(wynik),True,BLACK)
        screen.blit(text_surface,(SCREEN_WIDTH/2-150,10))

        ####################### aktualizacja
        player["rect"].update(player["x"], player["y"], player["picture"].get_width(), player["picture"].get_height())
        ciezar["rect"].update(ciezar["x"], ciezar["y"], ciezar["picture"].get_width(), ciezar["picture"].get_height())
        pygame.display.update()
        clock.tick(FPS)
        czas -= uplyw_czasu
        uplyw_czasu += 0.001
        #print("czas: ", czas)
        if czas <= 0:
            RUN = False
    return wynik
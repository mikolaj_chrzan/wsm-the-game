import pygame, sys, os
from funkcje import *
from config import *
def zapis_listy_slownikow_do_pliku(lista_zapisanych_danych, nazwa_pliku):
   #zapis danych do pliku
    with open(nazwa_pliku,"w") as uchwyt_pliku:
        #naglowek
        uchwyt_pliku.write("gracz ")
        slownik_gracz = lista_zapisanych_danych[0]
        for klucz in slownik_gracz:
            for wartosc in slownik_gracz[klucz]:
                uchwyt_pliku.write(wartosc+" ")
            uchwyt_pliku.write("\n")
            uchwyt_pliku.seek(0,2)
        #nazwa + rekordy
        for slownik1 in lista_zapisanych_danych:
            for nazwa in slownik1:
                uchwyt_pliku.write(nazwa+" ")
                #for naglowki in lista_zapisanych_danych[nazwa]:
                for kluczyk in slownik1[nazwa]:
                    wartosienka = slownik1[nazwa][kluczyk]
                    uchwyt_pliku.write(str(wartosienka)+" ")
                uchwyt_pliku.write("\n")
                uchwyt_pliku.seek(0,2)
def koniec_gry(nick,wyniki):
    #sumowanie wynikow
    wyniki["suma"] = int(wyniki["konk1"]) + int(wyniki["konk2"])
    wyniki["suma"] = int(wyniki["suma"])
    print(" WYNIK KONCOWY:  ", wyniki["suma"], "\n" * 5)
    #formatowanie slownika {gracz : {konkurencja1 : x, ... suma : X}}
    slownik_gracz = {}
    slownik_gracz[nick] = wyniki
    #pobieranie danych o rankingu i dodawanie informacji o odbytej grze
    lista_zapisanych_danych = plik_tabelka("zapis_odbytych_gier.txt")
    lista_zapisanych_danych = dodaj_malejacoB(lista_zapisanych_danych,slownik_gracz,"suma")
    zapis_listy_slownikow_do_pliku(lista_zapisanych_danych, "zapis_odbytych_gier.txt")

    #============================================================================================          EKRAN KONCOWY
    screen = pygame.display.set_mode((SCREEN_WIDTH, SCREEN_HEIGHT))
    pygame.display.set_caption("KONIEC GRY")
    resource_folder = os.path.join(os.path.dirname(__file__), "zasoby")

    wygrana_tlo = pygame.image.load(os.path.join(resource_folder, "wygrana.png"))
    przegrana_tlo = pygame.image.load(os.path.join(resource_folder, "przegrana.png"))

    pygame.font.init()
    informacja = "Statystyki"
    superczcionka = pygame.font.Font(None, 50)
    text_surface = superczcionka.render(informacja, True, BLACK)
    text_rect = (0, 0)
    #czy wygrales?
    czy_wygrana = False
    if slownik_gracz == lista_zapisanych_danych[0]:
        czy_wygrana = True
    RUN = True
    while RUN == True:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                RUN = False
                pygame.quit()
                sys.exit()
        keys = pygame.key.get_pressed()
        if keys[pygame.K_RETURN]:
            RUN = False
        ###############
        if czy_wygrana == True:
            screen.blit(wygrana_tlo, (0, 0))
        else:
            screen.blit(przegrana_tlo, (0,0))
        screen.blit(text_surface, text_rect)

        x_staty = 0
        y_staty = 50
        for listenki in lista_zapisanych_danych:
            for nicki in listenki:
                nickstring = str(nicki)
                screen.blit(superczcionka.render(nickstring,True,YELLOW),(x_staty,y_staty))
                wynikstring = str(listenki[nicki]["suma"])
                screen.blit(superczcionka.render(wynikstring,True,WHITE), (x_staty+300,y_staty))
                y_staty += 30
        pygame.display.update()
    #===================================================================================================================











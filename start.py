import pygame, sys, os
from config import *

def start_screen():
    screen = pygame.display.set_mode((SCREEN_WIDTH, SCREEN_HEIGHT))
    pygame.display.set_caption("WSM START SCREEN")
    clock = pygame.time.Clock()
    startfont = pygame.font.Font(None, 50)
    resource_folder = os.path.join(os.path.dirname(__file__), "zasoby")


    poczatkowy_pudzian = pygame.image.load(os.path.join(resource_folder, "groznystrongman.png"))
 #dodanie gwiazdek jako slownikow i wprawienie ich w ruch   gwiazdki = {}
    tytul = pygame.image.load(os.path.join(resource_folder, "start_tytul.png"))
    gwiazdki1 = pygame.image.load(os.path.join(resource_folder,"gwiazdki1.png"))
    gwiazdki2 = pygame.image.load(os.path.join(resource_folder, "gwiazdki2.png"))
    gwiazdki3 = pygame.image.load(os.path.join(resource_folder, "gwiazdki3.png"))

    # wyeliminowanie bialego tla z grafik
    tytul.set_colorkey(WHITE)
    gwiazdki1.set_colorkey(WHITE)
    gwiazdki2.set_colorkey(WHITE)
    gwiazdki3.set_colorkey(WHITE)

    #rect
    pudzian_rect = poczatkowy_pudzian.get_rect()
    text_surface = startfont.render(INSTRUKCJA_STARTOWA, True, RED)
    text_rect = (20,650)
    pudzian_x = 500

    ### ruch gwiazdki2 ###
    X_gwiazdki2 = 300
    Y_gwiazdki2 = 300
    ###########################################
    nazwa_gracza = ""
    RUN = True
    while RUN==True:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                RUN = False
                pygame.quit()
                sys.exit()
            if event.type == pygame.KEYDOWN and len(nazwa_gracza)<13:
                nazwa_gracza += event.unicode
        ##################### wyjscie z ekranu --> konkurencja1
        keys = pygame.key.get_pressed()
        if keys[pygame.K_RETURN]:
            RUN = False
            return nazwa_gracza
        ##################### grafiki
        screen.fill(SKY_BLUE)
        screen.blit(gwiazdki1, (100, 20))
        screen.blit(gwiazdki2,(X_gwiazdki2,Y_gwiazdki2))
        screen.blit(startfont.render(nazwa_gracza, True, PINK), (130, 130))
        screen.blit(poczatkowy_pudzian, (pudzian_x, SCREEN_HEIGHT - pudzian_rect.height))
        screen.blit(tytul, (0,0))
        ###################### text do wyswietlenia
        screen.blit(text_surface, text_rect)
        ###################### odswiezenie
        pygame.display.update()
        ###################### zmiany przed zapetleniem
        X_gwiazdki2 -= 1
        Y_gwiazdki2 -=0.2
        if pudzian_x > 150:
            pudzian_x -= 10
        ###################### FPS
        clock.tick(FPS)





